<#-- @ftlvariable name="pageTitle" type="String" -->
<#-- @ftlvariable name="htmlTitle" type="String" -->
<#-- @ftlvariable name="folders" type="java.util.List<dev.ruholnikov.htmlgenerator.beans.DiskFolderBean>" -->
<#-- @ftlvariable name="files" type="java.util.List<dev.ruholnikov.htmlgenerator.beans.DiskFileBean>" -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <h1>${htmlTitle}</h1>

    <link rel="stylesheet" href="/meta-info/bootstrap.min.css">
    <link href="/meta-info/css.css" rel="stylesheet">
    <link rel="stylesheet" href="/meta-info/baguetteBox.min.css">
    <link rel="stylesheet" href="/meta-info/fluid-gallery.css">

</head>
<body>

<div class="container gallery-container">
    <p class="page-description text-center">${pageTitle}</p>
    <div class="tz-gallery">

        <div class="row">

            <div class="row">
                <form action="/search">
                <input type="text" id="search-request" name="search-request">
                <input type="text" id="search-path" name="search-path" value="${pageTitle}" hidden/>
                <button type="submit">Search</button>
                </form>
            </div>

            <div class="none">
                <a href="/data/index.html">  <!--link to home -->
                    ./
                </a>
            </div>
            <#list folders as folder>
                <div class="none">
                    <a href="${folder.originFolderUrl + "index.html"}">  <!--folder itself -->
                        ${folder.folderName}
                    </a>
                </div>
            </#list>
        </div>
        <div class="row">
            <#list files as file>
                <#if file.isImage()>
                    <div class="col-sm-3 col-md-2">
                        <a class="lightbox" href="${"/" + file.originFileUrl}">  <!--image itself -->
                            <img src="${"/" + file.iconFileUrl}" alt="${file.fileName}"> <!--preview fule -->
                        </a>
                    </div>
                </#if>
            </#list>
        </div>
        <div class="row">
            <#list files as file>
                <#if file.isAudio()>
                    <div class="none">

                        <audio controls>
                            <source src="${"/" + file.iconFileUrl}" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                        <a href="${"/" + file.originFileUrl}">  <!--file itself -->
                            ${"/" + file.fileName}
                        </a>
                    </div>
                <#elseif file.isVideo()>
                    <div class="none">
                        <video width="320" controls>
                            <source src="${"/" + file.iconFileUrl}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                <#elseif file.isNotSupportedMediaFile()>
                    <div class="none">
                        <a href="${"/" + file.originFileUrl}">  <!--file itself -->
                            ${"/" + file.fileName}
                        </a>
                    </div>
                </#if>
            </#list>
        </div>

    </div>

</div>

<script src="/meta-info/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
</body>
</html>