package dev.ruholnikov.htmlgenerator;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.s3.event.S3EventNotification;
import dev.ruholnikov.htmlgenerator.beans.DiskFileBean;
import dev.ruholnikov.htmlgenerator.beans.DiskFolderBean;
import dev.ruholnikov.services.DynamoDBService;
import dev.ruholnikov.services.LambdaUtils;
import dev.ruholnikov.services.Logger;
import dev.ruholnikov.services.S3Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Handler implements RequestHandler<SNSEvent, String> {

    private Logger logger;
    private S3Service s3Service;
    private DynamoDBService dynamoDBService;

    public Handler() {
        logger = new Logger();
        s3Service = new S3Service();
        dynamoDBService = new DynamoDBService();
    }

    public Handler(Logger logger, S3Service s3Service, DynamoDBService dynamoDBService) {
        this.logger = logger;
        this.s3Service = s3Service;
        this.dynamoDBService = dynamoDBService;
    }

    @Override
    public String handleRequest(SNSEvent input, Context context) {
        for (SNSEvent.SNSRecord events : input.getRecords()) {
            S3EventNotification s3EventNotification = S3EventNotification.parseJson(events.getSNS().getMessage());
            String bucketName = s3EventNotification.getRecords().get(0).getS3().getBucket().getName();
            s3EventNotification.getRecords().stream()
                    .map(entity -> entity.getS3().getObject().getKey())
                    .map(LambdaUtils::decodeValue)
                    .filter(LambdaUtils::filterOutDataFiles)
                    .map(e -> e.substring(0, e.lastIndexOf('/')))
                    .collect(Collectors.toSet())
                    .forEach(folderKey -> {
                        processFolder(bucketName, folderKey);
                    });
        }
        return input.toString();
    }

    public void processFolder(String bucketName, String folderKey) {
        logger.debug("XXX update index.html for folder: " + folderKey);
        List<String> strings = s3Service.listObjectsInDir(bucketName, folderKey);
        List<String> dirs = s3Service.listCommonPrefixesInDirectory(bucketName, folderKey);
        List<DiskFolderBean> folders = dirs.stream()
                .filter(s -> s.replace(folderKey + "/", "").contains("/"))
                .filter(ss -> ss.endsWith("/"))
                .map(ee -> new DiskFolderBean("/" + ee))
                .collect(Collectors.toList());

        List<DiskFileBean> files = getFilesBean(folderKey, strings);
        files.addAll(getFilesBean(addRandomFile(folderKey)));
        logger.debug("XXX folders:");
        folders.forEach(f -> logger.debug(f.getOriginFolderUrl()));
        logger.debug("XXX ------------");
        logger.debug("XXX files:");
        files.forEach(f -> logger.debug(f.getOriginFileUrl()));
        logger.debug("XXX ------------");
        FreeMarkerUtil.updateIndex(bucketName, folderKey, s3Service, folders, files);
        //update parent folder
        if (folderKey.lastIndexOf('/') > 0) {
            processFolder(bucketName, folderKey.substring(0, folderKey.lastIndexOf('/')));
        }
    }

    private List<String> addRandomFile(String folderKey) {
        logger.debug("XXX adding random file");
        if(folderKey.equals(LambdaUtils.DATA_DIR.substring(0, LambdaUtils.DATA_DIR.length() - 1))){
            logger.debug("XXX current folder eligible for random file");
            String randomFile = dynamoDBService.getRandomFile();
            logger.debug("XXX file detected: "+ randomFile);
            if(randomFile != null) {
                return Collections.singletonList(randomFile);
            }
        }
        return Collections.emptyList();
    }

    private List<DiskFileBean> getFilesBean(String folderKey, List<String> strings) {
        return strings.stream()
                .filter(s -> !s.replace(folderKey + "/", "").contains("/"))
                .filter(ss -> !ss.endsWith("/"))
                .map(s -> new DiskFileBean(s, getIconUrl(s)))
                .peek(f -> logger.debug("XXX file will be added: " + f + " to folder: " + folderKey))
                .collect(Collectors.toList());
    }

    private List<DiskFileBean> getFilesBean(List<String> strings) {
        return strings.stream()
                .map(s -> new DiskFileBean(s, getIconUrl(s)))
                .peek(f -> logger.debug("XXX file will be added: " + f + " to folder: " + LambdaUtils.DATA_DIR))
                .collect(Collectors.toList());
    }

    private String getIconUrl(String s) {
        if (!LambdaUtils.filteringOutImagesFiles(s)) {
            return s;
        } else {
            return s.replace(LambdaUtils.DATA_DIR, LambdaUtils.ICON_DIR);
        }
    }

    public void setDynamoDBService(DynamoDBService service){
        this.dynamoDBService = service;
    }


    public Handler setLogger(Logger logger) {
        this.logger = logger;
        return this;
    }

    public Handler setS3Service(S3Service s3Service) {
        this.s3Service = s3Service;
        return this;
    }

}