package dev.ruholnikov.htmlgenerator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import dev.ruholnikov.htmlgenerator.beans.DiskFileBean;
import dev.ruholnikov.htmlgenerator.beans.DiskFolderBean;
import dev.ruholnikov.services.FileUtils;
import dev.ruholnikov.services.S3Service;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;

public class FreeMarkerUtil {

    public static final String TMP_INDEX_HTML = "/tmp/index.html";

    public static void main(String[] args) throws Exception {

        List<DiskFileBean> files = new ArrayList<>();
        DiskFileBean e = new DiskFileBean("sss", "ss");
        e.setOriginFileUrl("originUrl");
        e.setIconFileUrl("iconUFrl");
        files.add(e);
        files.add(e);
        List<DiskFolderBean> folders = new ArrayList<>();
        DiskFolderBean e1 = new DiskFolderBean("ss");
        e1.setOriginFolderUrl("folderUrl");
        folders.add(e1);
        folders.add(e1);

        createIndexFile(folders, files, "path");
    }

    public static void updateIndex(String bucketName, String folderPath, S3Service s3,
                                   List<DiskFolderBean> folders, List<DiskFileBean> files){
        FileUtils.removeFile(TMP_INDEX_HTML);
        createIndexFile(folders, files, folderPath);
        s3.uploadFile(bucketName, folderPath + "/index.html", TMP_INDEX_HTML);
    }

    private static void createIndexFile(List<DiskFolderBean> folders, List<DiskFileBean> files, String folderPath) {
        Configuration cfg = new Configuration();
        cfg.setClassForTemplateLoading(FreeMarkerUtil.class, "/templates");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        Map<String, Object> input = new HashMap<>();

        input.put("search_url_ref", System.getenv("SEARCH_URL_REF"));
        input.put("region_name", System.getenv("REGION_NAME"));
        input.put("htmlTitle", folderPath);
        input.put("pageTitle", folderPath);

        input.put("files", files);
        input.put("folders", folders);

        // 2.2. Get the template
        Template template;
        Writer fileWriter = null;
        try {
            template = cfg.getTemplate("index.ftl");
            FileUtils.removeFile(TMP_INDEX_HTML);
            fileWriter = new FileWriter(TMP_INDEX_HTML);
            template.process(input, fileWriter);
        } catch (IOException | TemplateException ex){
            ex.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
