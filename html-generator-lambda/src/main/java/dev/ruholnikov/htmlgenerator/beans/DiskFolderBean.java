package dev.ruholnikov.htmlgenerator.beans;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class DiskFolderBean {
    private String originFolderUrl;

    public DiskFolderBean(String originFolderUrl) {
        this.originFolderUrl = originFolderUrl;
    }

    public String getOriginFolderUrl() {
        return originFolderUrl;
    }

    public DiskFolderBean setOriginFolderUrl(String originFolderUrl) {
        this.originFolderUrl = originFolderUrl;
        return this;
    }

    public String getFolderName(){
        String[] split = originFolderUrl.split("/");
        List<String> collect = Arrays.asList(split).stream().filter(s -> s.trim().length() > 0).collect(Collectors.toList());
         return collect.get(collect.size()-1);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DiskFolderBean.class.getSimpleName() + "[", "]")
                .add("originFolderUrl='" + originFolderUrl + "'")
                .toString();
    }
}
