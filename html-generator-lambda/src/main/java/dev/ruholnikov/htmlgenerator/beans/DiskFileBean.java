package dev.ruholnikov.htmlgenerator.beans;

import dev.ruholnikov.services.LambdaUtils;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class DiskFileBean {
    private String originFileUrl;
    private String iconFileUrl;

    public DiskFileBean(String originFileUrl, String iconFileUrl) {
        this.originFileUrl = originFileUrl;
        this.iconFileUrl = iconFileUrl;
    }

    public String getOriginFileUrl() {
        return originFileUrl;
    }

    public DiskFileBean setOriginFileUrl(String originFileUrl) {
        this.originFileUrl = originFileUrl;
        return this;
    }

    public String getIconFileUrl() {
        return iconFileUrl;
    }

    public DiskFileBean setIconFileUrl(String iconFileUrl) {
        this.iconFileUrl = iconFileUrl;
        return this;
    }


    public boolean isImage(){
        return LambdaUtils.filteringOutImagesFiles(originFileUrl);
    }

    public boolean isAudio(){
        return LambdaUtils.filteringOutAudioFiles(originFileUrl);
    }

    public boolean isVideo(){
        return LambdaUtils.filteringOutVideoFiles(originFileUrl);
    }

    public boolean isNotSupportedMediaFile(){
        return !(isImage() || isAudio() || isVideo());
    }

    public String getFileName(){
        String[] split = originFileUrl.split("/");
        List<String> collect = Arrays.asList(split).stream().filter(s -> s.trim().length() > 0).collect(Collectors.toList());
        return collect.get(collect.size()-1);
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", DiskFileBean.class.getSimpleName() + "[", "]")
                .add("originFileUrl='" + originFileUrl + "'")
                .add("iconFileUrl='" + iconFileUrl + "'")
                .toString();
    }
}
