package dev.ruholnikov.htmlgenerator.beans;


import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class DiskFolderBeanTest {


    @Test
    public void getFolderName() {
        DiskFolderBean diskFolderBean = new DiskFolderBean("/tmp/key/something");
        diskFolderBean.setOriginFolderUrl("/tmp/key/something");
        assertEquals(diskFolderBean.getFolderName(), "something");
    }

    @Test
    public void getFolderNameEndingSlesh() {
        DiskFolderBean diskFolderBean = new DiskFolderBean("/tmp/key/something");
        diskFolderBean.setOriginFolderUrl("/tmp/key/something/");
        assertEquals(diskFolderBean.getFolderName(), "something");
    }
}
