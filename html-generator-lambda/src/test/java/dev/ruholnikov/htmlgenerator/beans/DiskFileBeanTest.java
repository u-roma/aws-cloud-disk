package dev.ruholnikov.htmlgenerator.beans;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class DiskFileBeanTest {


    @Test
    public void isImage() {
        DiskFileBean diskFileBean = new DiskFileBean("data/ss.jpg", "data/ss.jpg");
        assertTrue(diskFileBean.isImage());
        assertFalse(diskFileBean.isAudio());
        assertFalse(diskFileBean.isVideo());
        assertFalse(diskFileBean.isVideo());
    }

}
