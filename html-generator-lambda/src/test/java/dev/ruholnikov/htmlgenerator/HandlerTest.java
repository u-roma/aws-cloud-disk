package dev.ruholnikov.htmlgenerator;

import dev.ruholnikov.services.DynamoDBService;
import dev.ruholnikov.services.Logger;
import dev.ruholnikov.services.S3Service;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HandlerTest extends TestCase {
    @Mock
    private Logger logger;

    @Mock
    private S3Service s3Service;

    @Mock
    private DynamoDBService dynamoDBService;

    @InjectMocks
    private Handler handler;

    @Test
    public void testProcessFolderRoot() {
        when(s3Service.listObjectsInDir(anyString(), eq("data")))
                .thenReturn(Arrays.asList("file1.jpg","file2.jpg"));
        when(s3Service.listCommonPrefixesInDirectory(anyString(), eq("data")))
                .thenReturn(Arrays.asList("folder1","folder2"));
        when(dynamoDBService.getRandomFile()).thenReturn("data/test/file3.jpg");
        handler.processFolder("bucketName", "data");
    }
}