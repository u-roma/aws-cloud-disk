import json
import urllib.parse
import os
from decimal import *

import boto3
from sys import platform

print('Loading function')

if platform == "darwin":
    session = boto3.session.Session(profile_name="personal1")
else:
    session = boto3.session.Session()
s3 = session.client('s3')
rekognition = session.client('rekognition')
dynamodb = session.resource('dynamodb')
translate = session.client('translate')


def get_table_size(table: dynamodb.Table):
    scan = table.scan(Select='COUNT')
    print(json.dumps(scan, indent=4))
    return scan['Count']


def get_labels(bucket, key):
    response = rekognition.detect_labels(
        Image={
            'S3Object': {
                'Bucket': bucket,
                'Name': key
            }
        },
        MaxLabels=50,
        MinConfidence=70
    )
    return response['Labels']


def lambda_handler(event, context):
    dynamo_s3_table = os.environ.get('DYNAMO_TABLE')
    dynamo_counter_table = os.environ.get('DYNAMO_TABLE_COUNTER')
    for sns_event in event['Records']:
        s3_events = json.loads(sns_event['Sns']['Message'])
        for event_record in s3_events['Records']:
            bucket = event_record['s3']['bucket']['name']
            key = str(urllib.parse.unquote_plus(event_record['s3']['object']['key'], encoding='utf-8'))
            process_key(bucket, dynamo_counter_table, dynamo_s3_table, key)


def process_key(bucket, dynamo_counter_table, dynamo_s3_table, key):
    if not is_supported_image_file(key):
        return
    labels = get_labels(bucket, key)
    for label in labels:
        log_file_to_dynamo(bucket, dynamo_counter_table, dynamo_s3_table,
                           key, label['Name'], label['Confidence'])


def is_supported_image_file(key):
    return key.lower().endswith("jpg") or key.lower().endswith("jpeg") or key.lower().endswith("png")


def log_file_to_dynamo(bucket: str, dynamo_counter_table: str, dynamo_s3_table: str, key: str,
                       label_name: str, label_confidence):
    folder_path = get_folder_path(key)
    table = dynamodb.Table(dynamo_s3_table)
    table_counter = dynamodb.Table(dynamo_counter_table)

    get_updated_counter(table_counter, label_name)
    response = table.put_item(
        Item={
            'id': label_name,
            'confidence': Decimal(label_confidence),
            's3-key': key,
            's3-key-path': folder_path,
            'bucket': bucket
        }
    )


def get_folder_path(key_path: str) -> str:
    return key_path[0: key_path.rindex('/', 0)]


def get_updated_counter(table, label_name) -> int:
    try:
        response = table.update_item(
            Key={
                'counter': label_name
            },
            UpdateExpression="set countLogic = countLogic + :val",
            ExpressionAttributeValues={
                ':val': Decimal(1)
            },
            ReturnValues="UPDATED_NEW"
        )
        return response['Attributes']['countLogic']
    except Exception as ex:
        target_lang = os.environ.get('TRANSLATE_LANG')
        label_title = ""
        if target_lang is None or len(target_lang) < 2:
            label_title = label_name
        else:
            label_title = translate_phrase(target_lang, label_name)
        response = table.put_item(
            Item={
                'counter': label_name,
                'countLogic': Decimal(1),
                'label_title': label_title
            }
        )


def translate_phrase(target_lang, phrase):
    try:
        response = translate.translate_text(
            Text=phrase,
            SourceLanguageCode='en',
            TargetLanguageCode=target_lang
        )
        return response['TranslatedText']
    except Exception:
        return ""
