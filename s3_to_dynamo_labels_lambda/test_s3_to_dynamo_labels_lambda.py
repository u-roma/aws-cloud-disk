import unittest

import s3_to_dynamo_labels_lambda


class Test(unittest.TestCase):

    def get_all_s3_keys(bucket):
        """Get a list of all keys in an S3 bucket."""
        keys = []

        kwargs = {'Bucket': bucket}
        while True:
            resp = s3_to_dynamo_labels_lambda.s3.list_objects_v2(**kwargs)
            for obj in resp['Contents']:
                keys.append(obj['Key'])

            try:
                kwargs['ContinuationToken'] = resp['NextContinuationToken']
            except KeyError:
                break
            print(f"we have read {len(keys)} keys from s3...")
        return keys







