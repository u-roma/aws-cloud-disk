# Aws Cloud Disk

## About
AWS based Cloud Disc (something similar to Google Photos)
This project has the aim to have long term storage, but without:
 - great functionality that providers give us (like Dropbox, Google Drive, OneDrive, ... )
 - great price that they charge for additional functionality. 
 
 
Also, there are the next characteristics: 
- This storage supports "pay as you store" price. That means you pay only for the amount of data you store. If you use it, you pay a little extra for data transfer.
- The storage is infinitely scalable without prepaying.
- Simple UI oriented on media gallery

You can read more about how it is created and how it works at this Blog post:
https://ugolnikovroman.medium.com/aws-cloud-disc-serverless-media-gallery-5da69090fef2 

## Architecture
<img src='reference%20architecture.png' width="80%">

# Ho to build
## Prerequisites 
See ./gitlab-ci.yml

## Build commands
See ./gitlab-ci.yml
 

# Ho to deploy

## Prerequisites 
## Deploy commands 
See init commands from .gitlab-ci.yml

## Structure of folders:
```
/meta-info
/meta-info/icons/*
/meta-info/index.html   <-- index template. Based on this template all files will be moved
/meta-info/error.html   <-- error page
/data/*                 <-- disk files itself
```

# Final word
Apologies for mess in the project. I do not have time to make it shiny (maybe someone can motivate me to do so). 
I wanted to make it even more functional: 
  - add face recognition, with AWS Rekognition
  - add gallery feature: random image (as you see DynamoDb already receive list of all media object stored)
  - add Cloud Front distribution together with Authentication with Lambda Edge
