package dev.ruholnikov.services;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;

import java.util.Random;

public class DynamoDBService {
    private final Logger logger = new Logger();

    final AmazonDynamoDB dynamoDBClient = AmazonDynamoDBClientBuilder.standard()
            .build();
    final DynamoDB dynamoDB;
    final Table tableCount;
    final Table table;

    public DynamoDBService() {
        dynamoDB = new DynamoDB(dynamoDBClient);
        String dynamo_table_counter = System.getenv("DYNAMO_TABLE_COUNTER");
        tableCount = dynamoDB.getTable(dynamo_table_counter == null ? "error" : dynamo_table_counter);
        String dynamo_table = System.getenv("DYNAMO_TABLE");
        table = dynamoDB.getTable(dynamo_table == null ? "error" : dynamo_table);
    }

    public String getRandomFile(){
        Item id = table.getItem(new PrimaryKey("id", getRandomInt()));
        logger.debug("XXX item fetched: " + id);
        return id == null ? null : id.getString("s3-key");
    }

    private int getRandomInt(){
        int maxId = getMaxId();
        Random ran = new Random();
        int i = ran.nextInt(maxId);
        logger.debug("XXX random i calculated: " + i);
        return i;
    }

    private int getMaxId() {
        Item item = tableCount.getItem(new PrimaryKey("counter", "totalCount"));
        logger.debug("XXX get max counter from db: " + item.toString());
        return item
                .getInt("countLogic");
    }
}
