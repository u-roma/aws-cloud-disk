package dev.ruholnikov.services;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class S3Service {
    public static final String TEMP_FILE_FOLDER = "/tmp";
    private final Logger logger = new Logger();

    final AmazonS3 s3 = AmazonS3ClientBuilder.standard()
            .build();


    public void uploadFile(String bucketName, String objectKey, String filePath) {
        System.out.format("Uploading %s to S3 bucket %s...\n", filePath, bucketName);
        final AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .build();
        try {
            s3.putObject(bucketName, objectKey, new File(filePath));
        } catch (AmazonServiceException e) {
            logger.debug("Error: " + e.getMessage());
            System.err.println(e.getErrorMessage());
        }
    }

    public List<String> listObjectsInDir(String bucketName, String dirKey) {

        final AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .build();
        ObjectListing result = s3.listObjects(bucketName, dirKey);
        List<String> finalResult = getS3Keys(result.getObjectSummaries());
        while (result.isTruncated()) {
            result = s3.listNextBatchOfObjects(result);
            finalResult.addAll(getS3Keys(result.getObjectSummaries()));
        }
        return finalResult;
    }

    public List<String> listCommonPrefixesInDirectory(String bucketName, String prefix) {
        String delimiter = "/";
        if (!prefix.endsWith(delimiter)) {
            prefix += delimiter;
        }

        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(bucketName).withPrefix(prefix)
                .withDelimiter(delimiter);
        ObjectListing objects = s3.listObjects(listObjectsRequest);
        return objects.getCommonPrefixes();
    }

    private List<String> getS3Keys(List<S3ObjectSummary> objectSummaries) {
        return objectSummaries
                .stream()
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toList());
    }

    public void downloadFile(String bucketName, String objectKey, String filePath) {
        System.out.format("Downloading %s from S3 bucket %s...\n", objectKey, bucketName);
        final AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();
        try {
            S3Object o = s3.getObject(bucketName, objectKey);
            S3ObjectInputStream s3is = o.getObjectContent();
            FileUtils.removeFile(TEMP_FILE_FOLDER + filePath);
            FileOutputStream fos = new FileOutputStream(new File(TEMP_FILE_FOLDER + filePath));
            byte[] read_buf = new byte[1024];
            int read_len = 0;
            while ((read_len = s3is.read(read_buf)) > 0) {
                fos.write(read_buf, 0, read_len);
            }
            s3is.close();
            fos.close();
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        logger.debug("downloaded successfully: " + objectKey + " to " + TEMP_FILE_FOLDER + filePath);
    }

}
