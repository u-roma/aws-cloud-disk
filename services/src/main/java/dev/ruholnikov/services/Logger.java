package dev.ruholnikov.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    public boolean isDebugEnabled;

    public Logger() {
        String debug_mode = System.getenv("DEBUG");
        isDebugEnabled = new Boolean(debug_mode);
    }

    public void log(String message) {
        System.out.println(decorateMessage(message));
    }

    public void debug(String message) {
        if (isDebugEnabled) {
            System.out.println(decorateMessage(message));
        }
    }

    private boolean isUnix() {
        String os = System.getProperty("os.name");
        return (os.indexOf("nix") > 0 || os.indexOf("nux") > 0 || os.indexOf("aix") > 0);

    }

    /**
     * Decorate message with date time for local development
     */
    private String decorateMessage(String message) {
        if (!isUnix()) {
            return LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME) + " " + message;
        }
        return message;
    }
}
