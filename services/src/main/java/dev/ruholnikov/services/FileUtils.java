package dev.ruholnikov.services;

import java.io.File;

public class FileUtils {

    public static void removeFile(String path){
        File file = new File(path);
        if (file.exists()){
            file.delete();
        }
    }
}
