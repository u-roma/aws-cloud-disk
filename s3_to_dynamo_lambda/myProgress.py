import sys


def print_progress(index: int, total: int, message=''):
    count = index + 1
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '#' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s %s\r' % (bar, percents, '%', message))
    sys.stdout.flush()
    if count == total:
        sys.stdout.write("\n")
        sys.stdout.flush()


def print_progress_list(element, list_object: list, message=''):
    index = list_object.index(element)
    print_progress(index, len(list_object), message)
