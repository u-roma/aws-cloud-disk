import json
import urllib.parse
import os
from decimal import *

import boto3
from sys import platform

print('Loading function')

if platform == "darwin":
    session = boto3.session.Session(profile_name="personal1")
else:
    session = boto3.session.Session()
s3 = session.client('s3')
dynamodb = session.resource('dynamodb')


def get_table_size(table: dynamodb.Table):
    scan = table.scan(Select='COUNT')
    print(json.dumps(scan, indent=4))
    return scan['Count']


def lambda_handler(event, context):
    # print(f"received event: {event}")
    # Get the object from the event and show its content type
    dynamo_s3_table = os.environ.get('DYNAMO_TABLE')
    dynamo_counter_table = os.environ.get('DYNAMO_TABLE_COUNTER')
    for sns_event in event['Records']:
        s3_events = json.loads(sns_event['Sns']['Message'])
        for event_record in s3_events['Records']:
            bucket = event_record['s3']['bucket']['name']
            key = str(urllib.parse.unquote_plus(event_record['s3']['object']['key'], encoding='utf-8'))
            log_file_to_dynamo(bucket, dynamo_counter_table, dynamo_s3_table, key)


def log_file_to_dynamo(bucket: str, dynamo_counter_table: str, dynamo_s3_table: str, key: str):
    if not (key.lower().endswith("jpg") or key.lower().endswith("jpeg") or key.lower().endswith("png")):
        return
    folder_path = get_folder_path(key)
    table = dynamodb.Table(dynamo_s3_table)
    table_counter = dynamodb.Table(dynamo_counter_table)
    response = table.put_item(
        Item={
            'id': get_updated_counter(table_counter),
            's3-key': key,
            's3-key-path': folder_path,
            'bucket': bucket
        }
    )


def get_folder_path(key_path: str) -> str:
    return key_path[0: key_path.rindex('/', 0)]


def get_updated_counter(table) -> int:
    try:
        response = table.update_item(
            Key={
                'counter': 'totalCount'
            },
            UpdateExpression="set countLogic = countLogic + :val",
            ExpressionAttributeValues={
                ':val': Decimal(1)
            },
            ReturnValues="UPDATED_NEW"
        )
        return response['Attributes']['countLogic']
    except Exception as ex:
        response = table.put_item(
            Item={
                'counter': 'totalCount',
                'countLogic': Decimal(0)
            }
        )