package dev.ruholnikov.icongenerator;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.s3.event.S3EventNotification;
import dev.ruholnikov.services.LambdaUtils;
import dev.ruholnikov.services.Logger;
import dev.ruholnikov.services.S3Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static dev.ruholnikov.services.S3Service.TEMP_FILE_FOLDER;

public class Handler implements RequestHandler<SNSEvent, String> {

    public static final int MAX_WIDTH = 600;
    private final Logger logger = new Logger();

    private final S3Service s3Service;
    private final ImageService imageService;

    public Handler() {
        s3Service = new S3Service();
        imageService = new ImageService();
    }

    public Handler(S3Service s3Service, ImageService imageService) {
        this.s3Service = s3Service;
        this.imageService = imageService;
    }

    @Override
    public String handleRequest(SNSEvent input, Context context) {
        for (SNSEvent.SNSRecord events : input.getRecords()) {
            S3EventNotification s3EventNotification = S3EventNotification.parseJson(events.getSNS().getMessage());
            String bucketName = s3EventNotification.getRecords().get(0).getS3().getBucket().getName();
            s3EventNotification.getRecords().stream()
                    .map(s3Notification -> LambdaUtils.decodeValue(s3Notification.getS3().getObject().getKey()))
                    .filter(LambdaUtils::filteringOutImagesFiles)
                    .forEach(objectKey -> {
                        logger.debug("going to creat icon for :" + objectKey);
                        downloadFileToTmp(objectKey, bucketName);
                        makeIcon(MAX_WIDTH, objectKey, bucketName);
                        uploadFile(objectKey, bucketName);
                        logger.log("processed file: " + objectKey);
                    });
        }
        return null;
    }

    public void uploadFile(String key, String bucket) {
        String iconObjectKey = getIconObjectKey(key);
        String filePath = "/tmp" + getLocalFileName(key) + ".jpg";
        logger.debug(String.format("trying to upload %s file to %s:", filePath, iconObjectKey));
        s3Service.uploadFile(bucket, iconObjectKey, filePath);
    }


    public void makeIcon(int maxWidth, String key, String bucket) {
        BufferedImage bimg = null;
        try {
            bimg = ImageIO.read(new File(TEMP_FILE_FOLDER + getLocalFileName(key)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        int currentWidth = bimg.getWidth();
        int currentHeight = bimg.getHeight();
        try {
            logger.debug("resizing file");
            imageService.resize(TEMP_FILE_FOLDER + getLocalFileName(key),
                    TEMP_FILE_FOLDER + getLocalFileName(key) + ".jpg",
                    maxWidth, imageService.getHight(currentWidth, currentHeight, maxWidth));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void downloadFileToTmp(String key, String bucket) {
        s3Service.downloadFile(bucket, key, getLocalFileName(key));
    }

    private String getLocalFileName(String rowFileName) {
        return rowFileName.substring(rowFileName.lastIndexOf('/'));
    }

    public String getIconObjectKey(String s3ObjectKey) {
        return s3ObjectKey.replace(LambdaUtils.DATA_DIR, LambdaUtils.ICON_DIR);
    }

}
