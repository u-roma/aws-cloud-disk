package dev.ruholnikov.icongenerator;

import dev.ruholnikov.services.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageService {

    private final Logger logger = new Logger();

    /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     *
     * @param inputImagePath  Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param scaledWidth     absolute width in pixels
     * @param scaledHeight    absolute height in pixels
     * @throws IOException
     */
    public void resize(String inputImagePath,
                       String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        long x = System.currentTimeMillis();
        logger.debug("image was read:" + g2d.toString());
        logger.debug(String.format("Scale: w: %s h: %s", scaledWidth, scaledHeight));
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        logger.debug("writting " + inputImagePath + " to " + outputImagePath);
        ImageIO.write(outputImage, formatName, new File(outputImagePath));

        logger.debug(" init file exist: " + new File(inputImagePath).exists());
        logger.debug(" file created: " + new File(outputImagePath).exists());
        logger.debug("in: " + (System.currentTimeMillis() - x) / 1000.0 + " sec");
    }

    public int getHight(int currentWidth, int currenHight, int desiredWidth) {
        //1000  * 500
        //  10  *  5
        return desiredWidth * currenHight / currentWidth;
    }
}
