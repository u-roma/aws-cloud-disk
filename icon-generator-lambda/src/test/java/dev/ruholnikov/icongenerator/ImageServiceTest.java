package dev.ruholnikov.icongenerator;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ImageServiceTest {


    @Test
    public void getHight() {
        int hight = new ImageService().getHight(1000, 500, 10);
        assertEquals(5,hight);
    }

//    @Test
//    public void testPng() throws IOException {
//        new ImageService().resize("/Users/ruholnikov/Downloads/swimlanes-4d78e0a767a646f2bef40415d266f643.png", "/Users/ruholnikov/Downloads/png.jpg",100,100);
//    }
//    @Test heic is not supported, as there is no free heic decoder in java
//    public void testHeic() throws IOException {
//        new ImageService().resize("/Users/ruholnikov/Downloads/IMG_6805.HEIC", "/Users/ruholnikov/Downloads/heic.jpg", 100,100);
//    }
}
