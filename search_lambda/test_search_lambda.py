import json
import unittest
import urllib.parse
from unittest import TestCase

import search_lambda


class Test(unittest.TestCase):

    def test_lambda_handler(self):
        # print('Loading function')
        eventFile = json.loads(open('request.json').read())
        handler = search_lambda.lambda_handler(eventFile, None)
        print(handler)


    def test_get_images_html(self):
        s3_key_encoded = urllib.parse.quote("ыва ыы/ss")
        self.assertEqual(s3_key_encoded, "%D1%8B%D0%B2%D0%B0%20%D1%8B%D1%8B/ss")
