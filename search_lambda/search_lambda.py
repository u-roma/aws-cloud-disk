import os
import boto3
import urllib.parse
from sys import platform

print('Loading function')
home_url = os.environ.get('HOME_URL')
html_sceleton = '<html><body><!--search form--><a href={}>Home .</a> <form action="" method="get"><label for="site-search">Search image:</label><input type="search" id="site-search" name="search"aria-label="Search through image content"><button>Search</button><a href="?"><--</a></form><!--search form--><hr/>{}<hr/>{}</body></html>'

if platform == "darwin":
    session = boto3.session.Session(profile_name="personal1")
else:
    session = boto3.session.Session()
dynamodb = session.client('dynamodb')


def get_items_by_label(label, table_name):
    response = dynamodb.query(
        TableName=table_name,
        ConsistentRead=False,
        Limit=120,
        KeyConditions={
            'id': {
                'AttributeValueList': [
                    {
                        'S': label
                    },
                ],
                'ComparisonOperator': 'EQ'
            }
        },
    )
    return response['Items']


def get_top_labels(limit: int, table_name):
    response = dynamodb.scan(
        TableName=table_name,
        Limit=limit,
        Select='ALL_ATTRIBUTES',
        ReturnConsumedCapacity='TOTAL',
        ConsistentRead=False
    )
    return response


def get_images_html(items_by_label):
    html = ""
    for item in sorted(items_by_label, reverse=True, key=lambda i: float(i['confidence']['N'])):
        s3_key_encoded = urllib.parse.quote(item['s3-key']['S'])
        if s3_key_encoded.startswith("data/"):
            html = html + f"<a href=\"https://{item['bucket']['S']}.s3.{session.region_name}.amazonaws.com/{s3_key_encoded}\"> <img src=\"https://{item['bucket']['S']}.s3.{session.region_name}.amazonaws.com/meta-info/icons/{s3_key_encoded[5:]}\" width=\"300\" alt=\"Cat (confidence: {item['confidence']['N']})\"></a>"
    return f"<div>{html}</div>"


def get_labels_html(labels):
    html_items = ""
    for item in sorted(labels['Items'], reverse=True, key=lambda i: float(i['countLogic']['N'])):
        html_items = html_items + f"<a href=\"?search={item['counter']['S']}\">{item['label_title']['S']}({item['countLogic']['N']}) </a>"
    return f"<div>{html_items}</div>"


def lambda_handler(event, context):
    dynamo_labels_table = os.environ.get('DYNAMO_TABLE')
    dynamo_counter_table = os.environ.get('DYNAMO_TABLE_COUNTER')
    labels_html = ""
    images_html = ""
    try:
        search_parameter = event['queryStringParameters']['search']
        items_by_label = get_items_by_label(search_parameter, dynamo_labels_table)
        if len(items_by_label) < 1:
            labels = get_top_labels(1500, dynamo_counter_table)
            labels_html = get_labels_html(labels)
        else:
            labels_html = ""
        images_html = get_images_html(items_by_label)
    except Exception  as ex:
        labels = get_top_labels(1500, dynamo_counter_table)
        labels_html = get_labels_html(labels)

    response = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "text/html; charset=UTF-8"
        },
        "body": html_sceleton.format(home_url, labels_html, images_html),
        "isBase64Encoded": False
    }
    return response